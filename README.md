# hse-scripting-tools

Various scripting tools and snippets


## :bulb: Documentation

None yet.


## :rocket: Installation

No installation needed. Just execute the VIs or open the LV-projects if availale.


### :link: Dependencies

None.


### :wrench: LabVIEW 2016

The VIs are maintained in LabVIEW 2016.


## :busts_in_silhouette: Contributing 

Please get in touch with us at (office@hampel-soft.com) or visit our website (www.hampel-soft.com) if you want to contribute.


##  :beers: Credits

* Joerg Hampel
* Manuel Sebald
* Benjamin Hinrichs


## :page_facing_up: License 

This repository and its contents are licensed under a BSD/MIT like license - see the [LICENSE](LICENSE) file for details
